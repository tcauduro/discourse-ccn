# discourse-ccn

Script to import CCN members to WCC's Discourse instance.

Discourse API Documentation: https://docs.discourse.org/

## Installation

Clone the repository, create a virtual environment, and pip install requirements.

```bash
# clone git repo
git clone https://gitlab.com/tcauduro/discourse-ccn.git
cd discourse-ccn

# create virtual env if you don't want to install requirements globally
python3 -m venv venv

# activate virtual environment
venv/bin/activate

# install requirements
pip install -r requirements.txt
```

##### Environment Variables
You will need to set the following environment variables
Easiest method is to append the variables to the .profile file
```
# CCN Credentials
export CCN_USER='<user here>'
export CCN_PASS='<pass here>'
export CCN_REPORT_ID='<report id here>'

# Discourse Credentials
export DISCOURSE_USER='<user here>'
export DISCOURSE_KEY='<key here>'
export DISCOURSE_HOST='https://<forum host here>'
```
## Usage

##### Manual Execution

```bash
cd /home/wcc/discourse-ccn
source venv/bin/activate

python ccn.py

python discourse.py
```

##### Automation via Cron (as wcc user)
```bash
# Downloads CCN membership on the hour
0 * * * * /home/wcc/discourse-ccn/runscript ccn.py

# Updates Discourse groups 2 minutes later
2 * * * * /home/wcc/discourse-ccn/runscript ccn.py
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

from dotenv import load_dotenv
load_dotenv()
import csv
import datetime
import logging
import requests
from config import DISCOURSE as config
from pprint import pprint

logger = logging.getLogger(__name__)

headers = {
    'Api-Key': config['credentials']['key'],
    'Api-Username': config['credentials']['user'],
}

current_group_name = "club_members_" + str(datetime.date.today().year)

def get_discourse_users():
    """
    Queries discourse for a full list of active users
    Returns an array of dicts with user id and email
    Example:  [{id: 1, email: 'user@email.com'}]
    """
    global headers

    # Using custom query from data explorer plugin and API.
    # Look for membership_emails query with ID 2.
    endpoint = "/admin/plugins/explorer/queries/2/run"
    url = config['url']['host'] + endpoint

    r = requests.post(url, headers=headers)

    # make sure we get a response
    if r.status_code != 200:
        msg = f"Error retrieving users. {r.status_code}: {r.text}"
        logger.error(msg)
        raise Exception(msg)

    # parse the json response
    data = r.json()['rows']

    # extract email and user id into an array of dicts
    return [{'id': x[0], 'username': x[1], 'email': x[2]} for x in data]


def get_club_members(file):
    """
    Reads the members csv file
    Returns a dict of club members and their info
    """
    records = []
    # open source file in read only mode
    with open(file, newline='') as src_file:
        reader = csv.reader(src_file)
        # Process rows
        for index, row in enumerate(reader):
            # Skip headers and extra footer data
            if index == 0:
                continue

            # Pull out our desired fields
            records.append({
                'first': row[0],
                'last': row[1],
                'email': row[2],
                'sex': row[3],
                'dubs': row[4],
            })

    return records


def get_discourse_ccn_intersection():
    """
    Run an intersection on discourse users and club members
    This will check both a user's CCN email and Google email
    """
    discourse_users = get_discourse_users()
    club_members = get_club_members("members.csv")

    matches = []
    for u in discourse_users:
        for m in club_members:
            if u['email'].lower() == m['email'].lower():
                u['sex'] = m['sex']
                u['dubs'] = m['dubs']
                matches.append(u)
                break

    return matches


def get_group(group_name):
    """
    Retrieves the ID of the current year's membership group
    """
    global headers

    endpoint = f"/groups/{group_name}.json"
    url = config['url']['host'] + endpoint

    r = requests.get(url, headers=headers)
    if r.status_code == 200:
        group = r.json()['group']
        return group
    elif r.status_code == 404:
        return None
    msg = f"Error getting group. {r.status_code} {r.json()}"
    logger.error(msg)
    raise Exception(msg, r)


def create_club_members_group():
    """
    Creates the current year club member group
    """
    global headers
    date = datetime.date.today()

    # Club members group will have the current year.
    group_name = "club_members_" + str(date.year)
    endpoint = f"/admin/groups.json"
    url = config['url']['host'] + endpoint

    # Setup group settings
    group = {
        'name': group_name,
        'automatic': False,
        'automatic_membership_email_domains': 'waterloocyclingclub.ca',
        'allow_membership_requests': False,
        'allow_unknown_sender_topic_replies': False,
        'public_admission': False,
        'public_exit': False,
        'visibility_level': 3,  # Owner and mods only
        'members_visibility_level': 3,  # Owner and mods only
        'bio_cooked': f'<p>Paid WCC members {date.year}</p>',
        'bio_excerpt': f'Paid WCC members {date.year}',
        'bio_raw': f'Paid WCC members {date.year}',
        'flair_color': '000',
        'flair_icon': 'star',
        'flair_type': 'icon',
        'flair_url': 'star',
        'full_name': f'Club Members {date.year}',
        'grant_trust_level': 2,
        'mentionable': False,
        'title': f'Club Member {date.year}',
    }

    r = requests.post(url, headers=headers, json=group)
    if r.status_code == 200:
        return r.json()['basic_group']
    msg = f"Error creating group. Status: {r.status_code} Data: {r.json()}"
    logger.error(msg)
    raise Exception(msg, r)


def get_group_users(group_name):
    """
    Retrieves the current club_membership group membership
    """
    global headers
    params = {'limit': 1000}
    endpoint = f"/groups/{group_name}/members.json"
    url = config['url']['host'] + endpoint

    r = requests.get(url, headers=headers, params=params)
    if r.status_code == 200:
        return r.json()['members']
    msg = f"Error getting group membership for group {group_name}"
    logger.error(msg)
    raise Exception(msg, r)


def get_club_member_group_difference():
    """
    Run a difference check on discourse users and club members by id
    returns a list of users NOT in the club_members group
    """
    global current_group_name

    # Gets all discourse users found in the CCN membership list
    club_members = get_discourse_ccn_intersection()
    # Gets all members in the current club_members group
    group_members = get_group_users(current_group_name)
    for c in club_members.copy():
        for g in group_members:
            if c['id'] == g['id']:
                club_members.remove(c)
                break

    return club_members


def get_womens_group_difference():
    """
    Run a difference check on discourse users and womens group members by id
    returns a list of users NOT in the womens group
    """

    # Gets all discourse users found in the CCN membership list
    club_members = get_discourse_ccn_intersection()
    # Gets all members in the current club_members group
    group_members = get_group_users('women-club-members')
    for c in club_members.copy():
        if c['sex'] != 'Female' or c['dubs'] is True:
            club_members.remove(c)
            continue
        for g in group_members:
            if c['id'] == g['id']:
                club_members.remove(c)
                break
    return club_members


def add_users_to_group(group_id, user_list):
    """
    Adds users to the current club members group
    """
    global headers

    endpoint = f"/groups/{group_id}/members.json"
    url = config['url']['host'] + endpoint

    # Setup group settings
    users_to_add = {
        "emails": ''.join(f"{x['email']}," for x in user_list)
    }

    r = requests.put(url, headers=headers, json=users_to_add)
    if r.status_code == 200:
        return r.json()
    msg = f"Error adding users to group. Status: {r.status_code} Data: {r.json()}"
    logger.error(msg)
    raise Exception(msg, r)


if __name__ == '__main__':
    """ Running the module directly assumes members.csv exists.  This will update the groups accordingly. """
    logging.basicConfig(filename='discourse.log', filemode='a', format='%(asctime)s %(name)s - %(levelname)s - %(message)s')
    logging.getLogger().addHandler(logging.StreamHandler())
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Step 1 - Get the current club_members group.
    logger.info("Retrieving current year club_members group.")
    club_members_group = get_group(current_group_name)

    # Step 1.b - If there is no current club members group, create it.
    if club_members_group is None:
        logger.info("Current club_members group doesn't exist. Creating it.")
        club_members_group = create_club_members_group()
        logger.info("Current club_members group created.")

    # Step 2 - Find difference between current club members group and all club members found in discourse
    logger.info("Finding outstanding users to add to club_members group.")
    outstanding_users = get_club_member_group_difference()

    # Step 3 - Add outstanding users to current club members group
    if len(outstanding_users):
        logger.info("Adding outstanding users to club_members group.")
        users_added = add_users_to_group(club_members_group['id'], outstanding_users)
        logger.info(users_added['usernames'])
    else:
        logger.info("No outstanding users found.")

    # Step 4 - Same steps, but for women's group
    logger.info("Finding outstanding users to add to women's group.")
    outstanding_women = get_womens_group_difference()

    if len(outstanding_women):
        womens_group = get_group('women-club-members')

        logger.info("Adding outstanding users to women's group.")
        users_added = add_users_to_group(womens_group['id'], outstanding_women)
        logger.info(users_added['usernames'])
    else:
        logger.info("No oustanding users found.")
